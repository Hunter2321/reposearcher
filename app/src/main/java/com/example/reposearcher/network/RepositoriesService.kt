package com.example.reposearcher.network

import com.example.reposearcher.models.RepositoriesSearchResult
import com.example.reposearcher.models.Repository
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RepositoriesService {


    @GET("/repositories")
    fun getRepositoriesSince(
        @Query("since") since: Int,
        @Query("access_token") token: String = "d103a375fbd559e8ec4d4f59397689f77b46e768"

    ): Call<List<Repository>>

    @GET("/search/repositories")
    fun getRepositoriesByName(
        @Query("q") query: String,
        @Query("page") page:Int = 1,

        @Query("access_token") token: String = "d103a375fbd559e8ec4d4f59397689f77b46e768"
    ): Call<RepositoriesSearchResult>


    @GET("/repos/{ownerLogin}/{repoName}/languages")
    fun getListOfLanguages(
        @Path("ownerLogin") ownerLogin: String,
        @Path("repoName") repoName: String,

        @Query("access_token") token: String = "d103a375fbd559e8ec4d4f59397689f77b46e768"
    )
            : Call<Any>

    @GET("/repos/{ownerLogin}/{repoName}/stargazers")
    fun getStarsNumber(
        @Path("ownerLogin") ownerLogin: String,
        @Path("repoName") repoName: String,
        @Query("per_page") perPage: Int = 1,

        @Query("access_token") token: String = "d103a375fbd559e8ec4d4f59397689f77b46e768"
    )
            : Call<Any>
}
