package com.example.reposearcher.network

import android.os.Handler
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.example.reposearcher.models.Repository
import com.example.reposearcher.utils.HeadersUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ShowAllRepositoriesDataSource : PageKeyedDataSource<Int, Repository>(), DataSourceNetworkState {

    private val retrofit = RetrofitClientInstance.retrofitInstance
    private val getRepositoriesService = retrofit.create(RepositoriesService::class.java)

    private val networkState: MutableLiveData<NetworkState>? = MutableLiveData()

    override fun getNetworkState(): MutableLiveData<NetworkState>? {
        return networkState
    }

    var isHalfStarsFetched: Boolean = true
    var isHalfLanguagesFetched: Boolean = true

    lateinit var loadInitialCallback: LoadInitialCallback<Int, Repository>
    var loadAfterCallback: LoadCallback<Int, Repository>? = null


    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Repository>) {
        loadInitialCallback = callback
        loadInitial(0)
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Repository>) {
        loadAfterCallback = callback
        loadAfter(HeadersUtils.lastSeenRepository as Int)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Repository>) {
    }


    private fun loadInitial(lastSeenRepositoryId: Int) {
        networkState?.postValue(NetworkState.LOADING)

        getRepositoriesService.getRepositoriesSince(lastSeenRepositoryId).enqueue(object : Callback<List<Repository>> {
            override fun onFailure(call: Call<List<Repository>>, t: Throwable) {
                networkState?.postValue(NetworkState.LOADED)
            }

            override fun onResponse(call: Call<List<Repository>>, response: Response<List<Repository>>) {
                if (!response.isSuccessful) {
                    if(response.code() == 403){
                        networkState?.postValue(NetworkState.FORBIDDEN)
                    }
                    return
                }
                val responseBody = response.body() as MutableList<Repository>

                val headersLinkUtils = HeadersUtils(response.headers())
                headersLinkUtils.getFirstRepositoryIdOnNextPage()

                getLanguages(responseBody)
                getNumberOfStars(responseBody)

                loadInitCallbackAfterHalfItemsLoaded(responseBody)
            }
        })
    }


    private fun loadAfter(lastSeenRepositoryId: Int) {
        networkState?.postValue(NetworkState.LOADING)

        getRepositoriesService.getRepositoriesSince(lastSeenRepositoryId).enqueue(object : Callback<List<Repository>> {
            override fun onFailure(call: Call<List<Repository>>, t: Throwable) {
                networkState?.postValue(NetworkState.LOADED)

            }

            override fun onResponse(call: Call<List<Repository>>, response: Response<List<Repository>>) {
                if (!response.isSuccessful) {
                    if(response.code() == 403){
                        networkState?.postValue(NetworkState.FORBIDDEN)
                    }
                    return
                }
                val responseBody = response.body() as MutableList<Repository>

                val headersLinkUtils = HeadersUtils(response.headers())
                headersLinkUtils.getFirstRepositoryIdOnNextPage()


                getLanguages(responseBody)
                getNumberOfStars(responseBody)

                loadAfterCallbackAfterHalfItemsLoaded(responseBody)
            }
        })
    }


    private fun getLanguages(repos: List<Repository>) {
        isHalfLanguagesFetched = false
        for (repo in repos) {

            getRepositoriesService.getListOfLanguages(repo.owner.login, repo.name as String).enqueue(object :
                Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    if (!response.isSuccessful) {
                        if(response.code() == 403){
                            networkState?.postValue(NetworkState.FORBIDDEN)

                        }
                        return
                    }
                    val jsonResponse = Gson().toJson(response.body())
                    val empMapType = object : TypeToken<Map<String, Any>>() {}.type
                    val map: Map<String, Any> = Gson().fromJson(jsonResponse, empMapType)

                    val languages = mutableListOf<String>()
                    for (entry in map) {
                        languages.add("${entry.key}")
                    }
                    repo.languages = languages.joinToString(", ")

                    if((repos.size/2)==(repos.indexOf(repo))){
                        isHalfLanguagesFetched = true

                    }
                }
            })
            if(networkState?.value == NetworkState.FORBIDDEN){
                break
            }

        }
    }

    private fun getNumberOfStars(repos: List<Repository>) {
        isHalfStarsFetched = false
        for (repo in repos) {

            getRepositoriesService.getStarsNumber(repo.owner.login, repo.name as String)
                .enqueue(object : Callback<Any> {
                    override fun onFailure(call: Call<Any>, t: Throwable) {
                    }

                    override fun onResponse(call: Call<Any>, response: Response<Any>) {
                        if (!response.isSuccessful) {
                            if(response.code() == 403){
                                networkState?.postValue(NetworkState.FORBIDDEN)
                            }
                            return
                        }
                        val headersLinkUtils = HeadersUtils(response.headers())

                        if (headersLinkUtils.getNumberOfStars() != null) {
                            repo.stars = headersLinkUtils.getNumberOfStars() as Int
                        }
                        if((repos.size/2)==(repos.indexOf(repo))){
                            isHalfStarsFetched = true
                        }
                    }
                })
            if(networkState?.value == NetworkState.FORBIDDEN){
                break
            }

        }
    }

    fun loadInitCallbackAfterHalfItemsLoaded(responseBody: MutableList<Repository>){
        if(isHalfLanguagesFetched && isHalfStarsFetched){
            networkState?.postValue(NetworkState.LOADED)
            loadInitialCallback.onResult(responseBody, null, HeadersUtils.lastSeenRepository)
        } else Handler().postDelayed(Runnable { loadInitCallbackAfterHalfItemsLoaded(responseBody) }, 50)
    }
    fun loadAfterCallbackAfterHalfItemsLoaded(responseBody: MutableList<Repository>){
        if(isHalfLanguagesFetched && isHalfStarsFetched){
            networkState?.postValue(NetworkState.LOADED)
            loadAfterCallback?.onResult(responseBody, HeadersUtils.lastSeenRepository)
        } else Handler().postDelayed(Runnable { loadAfterCallbackAfterHalfItemsLoaded(responseBody) },50)
    }

}