package com.example.reposearcher.network

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import androidx.paging.ItemKeyedDataSource
import androidx.paging.PageKeyedDataSource
import com.example.reposearcher.models.Repository

class PagedDataSourceFactory(private val isItSearchRequest : Boolean,private val searchQuery : String? = null) : DataSource.Factory<Int,Repository>() {

    companion object {
        var networkStatusLiveData = MutableLiveData <DataSourceNetworkState>()
    }

    var repositoryDataSource : PageKeyedDataSource<Int, Repository>? = null

    override fun create(): DataSource<Int, Repository> {

        if(isItSearchRequest && searchQuery !=null){
            repositoryDataSource = SearchRepositoryByNameDataSource(searchQuery)

        } else {
                repositoryDataSource = ShowAllRepositoriesDataSource()
        }
        networkStatusLiveData.postValue(repositoryDataSource as DataSourceNetworkState)
        return repositoryDataSource as PageKeyedDataSource<Int, Repository>
    }

}