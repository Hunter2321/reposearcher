package com.example.reposearcher.network

enum class NetworkState {
    LOADING,LOADED, FORBIDDEN
}