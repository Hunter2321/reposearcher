package com.example.reposearcher.network

import android.os.Handler
import android.widget.Toast
import androidx.paging.PageKeyedDataSource
import com.example.reposearcher.models.RepositoriesSearchResult
import com.example.reposearcher.models.Repository
import com.example.reposearcher.utils.HeadersUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import androidx.lifecycle.MutableLiveData


class SearchRepositoryByNameDataSource(query: String) : PageKeyedDataSource<Int, Repository>(), DataSourceNetworkState {

    private val networkState: MutableLiveData<NetworkState>? = MutableLiveData()
    override fun getNetworkState(): MutableLiveData<NetworkState>? {
        return networkState
    }

    private val IN_QUALIFIER = "in:name"
    val query = query + IN_QUALIFIER

    lateinit var loadInitialCallback: LoadInitialCallback<Int, Repository>
    var loadAfterCallback: LoadCallback<Int, Repository>? = null

    var isHalfStarsFetched: Boolean = true
    var isHalfLanguagesFetched: Boolean = true

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, Repository>) {
        loadInitialCallback = callback
        loadInitial()
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Repository>) {
        loadAfterCallback = callback
        loadAfter(HeadersUtils.nextPageSearchResult)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Repository>) {

    }

    private val retrofit = RetrofitClientInstance.retrofitInstance
    private val getRepositoriesService = retrofit.create(RepositoriesService::class.java)


    private fun loadInitial() {
        networkState?.postValue(NetworkState.LOADING)
        getRepositoriesService.getRepositoriesByName(query).enqueue(object : Callback<RepositoriesSearchResult> {


            override fun onResponse(
                call: Call<RepositoriesSearchResult>,
                response: Response<RepositoriesSearchResult>
            ) {
                if (!response.isSuccessful) {
                    if (response.code() == 403) {
                        networkState?.postValue(NetworkState.FORBIDDEN)
                    }
                    return
                }

                val responseBody = response.body() as RepositoriesSearchResult

                val repos = responseBody.items

                HeadersUtils(response.headers()).getNextPageSearchResult()
                getLanguages(repos)
                getNumberOfStars(repos)

                loadInitCallbackAfterHalfItemsLoaded(repos)

            }

            override fun onFailure(call: Call<RepositoriesSearchResult>, t: Throwable) {
                networkState?.postValue(NetworkState.LOADED)
            }
        })
    }


    private fun loadAfter(nextPageSearchResult: Int) {
        networkState?.postValue(NetworkState.LOADING)

        getRepositoriesService.getRepositoriesByName(query = query, page = nextPageSearchResult)
            .enqueue(object : Callback<RepositoriesSearchResult> {
                override fun onFailure(call: Call<RepositoriesSearchResult>, t: Throwable) {
                    networkState?.postValue(NetworkState.LOADED)
                }

                override fun onResponse(
                    call: Call<RepositoriesSearchResult>,
                    response: Response<RepositoriesSearchResult>
                ) {
                    if (!response.isSuccessful) {
                        if (response.code() == 403) {
                            networkState?.postValue(NetworkState.FORBIDDEN)
                        }
                        return
                    }

                    val responseBody = response.body() as RepositoriesSearchResult
                    val repos = responseBody.items
                    HeadersUtils(response.headers()).getNextPageSearchResult()

                    getLanguages(repos)
                    getNumberOfStars(repos)

                    loadAfterCallbackAfterHalfItemsLoaded(repos)

                }
            })
    }


    private fun getLanguages(repos: MutableList<Repository>) {
        isHalfLanguagesFetched = false
        for (repo in repos) {
            getRepositoriesService.getListOfLanguages(repo.owner.login, repo.name as String).enqueue(object :
                Callback<Any> {
                override fun onFailure(call: Call<Any>, t: Throwable) {
                }

                override fun onResponse(call: Call<Any>, response: Response<Any>) {
                    if (!response.isSuccessful) {
                        if (response.code() == 403) {
                            networkState?.postValue(NetworkState.FORBIDDEN)
                        }
                        return
                    }
                    val jsonResponse = Gson().toJson(response.body())
                    val empMapType = object : TypeToken<Map<String, Any>>() {}.type
                    val map: Map<String, Any> = Gson().fromJson(jsonResponse, empMapType)

                    val languages = mutableListOf<String>()
                    for (entry in map) {
                        languages.add("${entry.key}")
                    }
                    repo.languages = languages.joinToString(", ")

                    if ((repos.size / 2) == (repos.indexOf(repo))) {
                        isHalfLanguagesFetched = true
                    }
                }
            })
            if (networkState?.value == NetworkState.FORBIDDEN) {
                println("LOG - its forbidden!")
                break
            }
        }
    }

    private fun getNumberOfStars(repos: MutableList<Repository>) {
        isHalfStarsFetched = false
        for (repo in repos) {
            getRepositoriesService.getStarsNumber(repo.owner.login, repo.name as String)
                .enqueue(object : Callback<Any> {
                    override fun onFailure(call: Call<Any>, t: Throwable) {
                    }

                    override fun onResponse(call: Call<Any>, response: Response<Any>) {
                        if (!response.isSuccessful) {
                            if (response.code() == 403) {
                                networkState?.postValue(NetworkState.FORBIDDEN)
                            }
                            return
                        }
                        val headersLinkUtils = HeadersUtils(response.headers())

                        if (headersLinkUtils.getNumberOfStars() != null) {
                            repo.stars = headersLinkUtils.getNumberOfStars() as Int
                        }

                        if ((repos.size / 2) == (repos.indexOf(repo))) {
                            isHalfStarsFetched = true
                        }
                    }
                })
            if (networkState?.value == NetworkState.FORBIDDEN) {
                println("LOG - its forbidden!")
                break
            }
        }
    }

    fun loadInitCallbackAfterHalfItemsLoaded(responseBody: MutableList<Repository>) {
        if (isHalfLanguagesFetched && isHalfStarsFetched) {
            networkState?.postValue(NetworkState.LOADED)
            loadInitialCallback.onResult(responseBody, null, HeadersUtils.nextPageSearchResult)
        } else Handler().postDelayed(Runnable { loadInitCallbackAfterHalfItemsLoaded(responseBody) }, 50)
    }

    fun loadAfterCallbackAfterHalfItemsLoaded(responseBody: MutableList<Repository>) {
        if (isHalfLanguagesFetched && isHalfStarsFetched) {
            networkState?.postValue(NetworkState.LOADED)
            loadAfterCallback?.onResult(responseBody, HeadersUtils.nextPageSearchResult)
        } else Handler().postDelayed(Runnable { loadAfterCallbackAfterHalfItemsLoaded(responseBody) }, 50)
    }
}