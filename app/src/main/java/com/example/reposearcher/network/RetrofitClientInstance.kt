package com.example.reposearcher.network

import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit




object RetrofitClientInstance {

    private var retrofit: Retrofit? = null
    private val BASE_URL = "https://api.github.com"

    val retrofitInstance: Retrofit
        get() {
            if (retrofit == null) {
                retrofit = retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit as Retrofit
        }
}