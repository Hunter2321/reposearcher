package com.example.reposearcher.network

import androidx.lifecycle.MutableLiveData

interface DataSourceNetworkState {

    fun getNetworkState(): MutableLiveData<NetworkState>?
}