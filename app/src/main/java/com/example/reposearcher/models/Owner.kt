package com.example.reposearcher.models

import com.google.gson.annotations.SerializedName

data class Owner (@SerializedName ("avatar_url") val avatar : String, @SerializedName("login") val login : String)