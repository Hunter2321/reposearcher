package com.example.reposearcher.models

import com.google.gson.annotations.SerializedName

data class Repository(
    @SerializedName("id") val id : Int,
    @SerializedName("name") var name: String? = null,
    @SerializedName("owner") var owner:Owner,
    @SerializedName("stars") var stars: Int = 0,
    var languages: String = "",
    @SerializedName("html_url") var link : String
)