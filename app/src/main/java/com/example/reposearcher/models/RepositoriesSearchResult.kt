package com.example.reposearcher.models

import com.google.gson.annotations.SerializedName

data class RepositoriesSearchResult(
    @SerializedName("items") val items: MutableList<Repository> = mutableListOf()
)

