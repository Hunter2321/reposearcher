package com.example.reposearcher.utils

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import com.example.reposearcher.R


class ProgressDialogService {

    var progressDialog: ProgressDialog? = null
    fun create(context: Context): ProgressDialog? {
        progressDialog = ProgressDialog(context)
        progressDialog?.setCancelable(false)
        return progressDialog
    }

    fun setMessage(message: String): ProgressDialog? {
        progressDialog?.let {
            progressDialog!!.setMessage(message)
        }
        return progressDialog
    }

    fun showDialog() {
        progressDialog?.let {
            progressDialog!!.show()
        }
    }

    fun hideDialog() {
        progressDialog?.let {
            progressDialog!!.hide()
            progressDialog = null
        }
    }
}


