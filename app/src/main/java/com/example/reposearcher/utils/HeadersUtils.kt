package com.example.reposearcher.utils

import okhttp3.Headers


class HeadersUtils(val headers: Headers) {

    companion object {
        var lastSeenRepository: Int? = 0
        var nextPageSearchResult: Int = 0
    }

    private val DELIM_LINKS = "," //$NON-NLS-1$

    private val DELIM_LINK_PARAM = ";" //$NON-NLS-1$

    private val HEADER_LINK = "Link"
    private val META_REL = "rel"

    private val META_LAST = "last"
    private val META_NEXT = "next"

    private var last: String? = null
    private var next: String? = null

    private fun initializeSplitLink() {

        val linkHeader: String? = headers.get(HEADER_LINK)

        if (linkHeader != null) {
            val links = linkHeader.split(DELIM_LINKS)

            for (link in links) {
                val segments = link.split(DELIM_LINK_PARAM)
                if (segments.size < 2)
                    continue

                var linkPart = segments[0].trim()

                if (!linkPart.startsWith("<") || !linkPart.endsWith(">"))  //$NON-NLS-1$ //$NON-NLS-2$
                    continue
                linkPart = linkPart.substring(1, linkPart.length - 1)


                for (i in 1 until segments.size) {
                    val rel = segments[i].trim().split("=") //$NON-NLS-1$
                    if (rel.size < 2 || !META_REL.equals(rel[0]))
                        continue

                    var relValue = rel[1]
                    if (relValue.startsWith("\"") && relValue.endsWith("\"")) //$NON-NLS-1$ //$NON-NLS-2$
                        relValue = relValue.substring(1, relValue.length - 1)


                    if (META_LAST.equals(relValue))
                        last = linkPart;
                    else if (META_NEXT.equals(relValue))
                        next = linkPart;

                }
            }
        }
    }


    val findNumbers = "\\d+".toRegex()

    fun getNumberOfStars(): Int? {
        initializeSplitLink()
        if (last != null) {
            val splitedLink = last?.split("page=")?.get(2)
            return findNumbers.find(splitedLink as CharSequence)?.value?.toInt()

        } else {
            return null
        }
    }

    fun getFirstRepositoryIdOnNextPage() {
        initializeSplitLink()
        if (next != null) {
            val numberPart = next?.split("since=")?.get(1)
            lastSeenRepository = findNumbers.find(numberPart as CharSequence)?.value?.toInt()
        }

    }

    fun getNextPageSearchResult() {
        initializeSplitLink()

        if (next != null) {
            val numberPart = next?.split("page=")?.get(1)

            if (findNumbers.find(numberPart as CharSequence)?.value?.toInt() != null)
                nextPageSearchResult = findNumbers.find(numberPart as CharSequence)?.value?.toInt() as Int
        }
    }


}

