package com.example.reposearcher.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.reposearcher.models.Repository
import de.hdodenhof.circleimageview.CircleImageView
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri
import androidx.paging.PagedList
import com.example.reposearcher.R


class RepositoryPagedListAdapter(val context:Context) : PagedListAdapter<Repository, RepositoryPagedListAdapter.MyViewHolder>(REPO_COMPARATOR) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.repositories_list_item, parent, false)
        val myViewHolder = MyViewHolder(v)

        return myViewHolder
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val repo = getItem(position)

        holder.nameTextView.text = repo?.name
        holder.languagesTextView.text = repo?.languages
        holder.starsTextView.text = repo?.stars.toString()

        Glide.with(context).load(repo?.owner?.avatar).into(holder.ownerAvatarImageView)

        holder.repositoryListItem.setOnClickListener {
            val url = repo?.link
            val i = Intent(Intent.ACTION_VIEW)
            i.data = Uri.parse(url)
            context.startActivity(i)
        }


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val repositoryListItem = itemView.findViewById<ConstraintLayout>(R.id.repository_list_item)

        val nameTextView = itemView.findViewById<TextView>(R.id.repositoryNameTextView)
        val languagesTextView = itemView.findViewById<TextView>(R.id.languagesTextView)

        val starsTextView = itemView.findViewById<TextView>(R.id.starsTextView)
        val ownerAvatarImageView = itemView.findViewById<CircleImageView>(R.id.ownerAvatarImageView)

    }


    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<Repository>() {
            override fun areItemsTheSame(oldItem: Repository, newItem: Repository): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Repository, newItem: Repository): Boolean =
                oldItem == newItem
        }
    }
}