package com.example.reposearcher.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.reposearcher.R
import com.example.reposearcher.adapter.RepositoryPagedListAdapter
import com.example.reposearcher.models.Repository
import com.example.reposearcher.network.NetworkState
import com.example.reposearcher.utils.ProgressDialogService
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MainActivityViewModel
    lateinit var adapter: RepositoryPagedListAdapter

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.action_bar_menu, menu)

        val searchItem = menu?.findItem(R.id.search_action)
        val searchView = searchItem?.actionView as SearchView


        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {

                viewModel.createAllRepoLiveData()
                bindRepoResultObservable()

                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem): Boolean {

                return true
            }
        })

        searchView.setOnQueryTextListener(
            object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    query?.let {
                        viewModel.createSearchLiveData(query)
                        bindRepoResultObservable()
                    }
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }
            })

        return super.onCreateOptionsMenu(menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.my_toolbar))

        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)


        adapter = RepositoryPagedListAdapter(this)
        repositoriesRecyclerView.layoutManager = LinearLayoutManager(this)
        repositoriesRecyclerView.setHasFixedSize(true)
        repositoriesRecyclerView.adapter = adapter

        bindRepoResultObservable()
        bindNetworkStatusObservable()
    }

    val listObserver = object : Observer<PagedList<Repository>> {
        override fun onChanged(t: PagedList<Repository>?) {
            adapter.submitList(t)
        }
    }


    fun bindRepoResultObservable() {
        viewModel.repoResult.observe(this, listObserver)
    }

    val progressDialogService = ProgressDialogService()

    fun bindNetworkStatusObservable() {
        viewModel.networkState.observe(this, Observer {


            if (it == NetworkState.LOADING) {

                progressDialogService.create(this)
                progressDialogService.setMessage("Content is loading...")
                progressDialogService.showDialog()

            } else {
                progressDialogService.hideDialog()
            }

            if (it == NetworkState.FORBIDDEN) {
                Toast.makeText(this, "Query limit is reached, try again later...", Toast.LENGTH_SHORT).show()
            }
        })
    }


}
