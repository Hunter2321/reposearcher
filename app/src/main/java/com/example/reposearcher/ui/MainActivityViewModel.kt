package com.example.reposearcher.ui

import androidx.lifecycle.*
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.example.reposearcher.network.PagedDataSourceFactory
import com.example.reposearcher.models.Repository

class MainActivityViewModel : ViewModel() {
    private val pagedListConfig = PagedList.Config.Builder().setEnablePlaceholders(false).setPageSize(20).build()
    private var repoFactory = PagedDataSourceFactory(false)


    var repoResult: LiveData<PagedList<Repository>> = LivePagedListBuilder<Int, Repository>(
        repoFactory, pagedListConfig
    ).build()

    fun createAllRepoLiveData() {
        repoFactory = PagedDataSourceFactory(false)
        repoResult = LivePagedListBuilder<Int, Repository>(repoFactory, pagedListConfig).build()

    }

    fun createSearchLiveData(query: String) {
        repoFactory = PagedDataSourceFactory(true, query)
        repoResult = LivePagedListBuilder<Int, Repository>(repoFactory, pagedListConfig).build()
    }

    val networkState = Transformations.switchMap(PagedDataSourceFactory.networkStatusLiveData) { dataSource ->
        return@switchMap dataSource.getNetworkState()
    }


}